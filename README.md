# apkcircledep

a simple utility built on [alpine/go][1] to find circular dependencies in aports

### usage

```console
$ go install gitlab.alpinelinux.org/ptrcnull/apkcircledep@latest
$ cd aports
$ apkcircledep
```

[1]: https://gitlab.alpinelinux.org/alpine/go