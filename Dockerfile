FROM golang:1.20.1-alpine3.17 AS builder
RUN apk add cmd:strip
WORKDIR /app

COPY go.mod go.sum .
RUN go mod download

COPY . .
RUN go build -o apkcircledep .
RUN strip apkcircledep

FROM scratch
COPY --from=builder /app/apkcircledep /
CMD ["/apkcircledep"]
