package main

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"runtime"
	"sort"
	"strings"
	"sync"

	"git.ddd.rip/ptrcnull/workers"
	"gitlab.alpinelinux.org/alpine/go/apkbuild"
	"mvdan.cc/sh/v3/expand"
)

type Entry struct {
	path  string
	entry fs.DirEntry
}

// booooo global variables
var dependencies = map[string][]string{}
var subpackages = map[string]string{}
var providers = map[string][]string{}
var checked = map[string]bool{}
var subMutex sync.Mutex
var depMutex sync.Mutex
var exitCode = 0

func main() {
	shellEnv := expand.ListEnviron(os.Environ()...)

	fmt.Println("parsing apkbuilds...")

	dirChan := traverse()
	workers.Channel(runtime.NumCPU(), dirChan, func(dir *Entry) {
		path := filepath.Join(dir.path, "APKBUILD")
		f, err := os.Open(path)
		if err != nil {
			if !errors.Is(err, fs.ErrNotExist) {
				fmt.Printf("open %s: %s\n", path, err)
				os.Exit(1)
			}
			return
		}
		origin := dir.entry.Name()

		apkbld, err := apkbuild.Parse(apkbuild.NewApkbuildFile(origin, f), shellEnv)
		if err != nil {
			fmt.Printf("error parsing %s: %s\n", origin, err)
			return
		}

		subMutex.Lock()
		for _, subpkg := range apkbld.Subpackages {
			subpackages[subpkg.Subpkgname] = origin
			// FIXME: get proper dependencies of subpackages?
			// https://gitlab.alpinelinux.org/alpine/go/-/issues/3
		}

		for _, provide := range apkbld.Provides {
			providers[provide.Pkgname] = append(providers[provide.Pkgname], origin)
		}
		subMutex.Unlock()

		alldepends := []string{}
		for _, depend := range apkbld.Depends {
			if !depend.Conflicts {
				alldepends = append(alldepends, depend.Pkgname)
			}
		}
		for _, depend := range apkbld.Makedepends {
			if !depend.Conflicts {
				alldepends = append(alldepends, depend.Pkgname)
			}
		}
		// special case: bison checkdepends are ignored during bootstrap,
		// because it depends on itself and flex (which depends on bison)
		if !apkbld.Options.Has("!check") && origin != "bison" {
			for _, depend := range apkbld.Checkdepends {
				if !depend.Conflicts {
					alldepends = append(alldepends, depend.Pkgname)
				}
			}
		}
		depMutex.Lock()
		dependencies[origin] = alldepends
		depMutex.Unlock()
	})

	fmt.Println("parsed, starting the search")

	pkgs := make([]string, 0, len(dependencies))
	for pkg := range dependencies {
		pkgs = append(pkgs, pkg)
	}
	sort.Strings(pkgs)

	// resolve subpackage and provider names to origins
	for _, pkg := range pkgs {
		deps := dependencies[pkg]
		for i, dep := range deps {
			if origin, ok := subpackages[dep]; ok {
				deps[i] = origin
			} else if providers, ok := providers[dep]; ok {
				// by default, pick the first provider from the list, alphabetically
				sort.Strings(providers)
				chosenProvider := providers[0]

				// however, if there's a provider with -stage0 or -bootstrap, prefer that instead
				// this is NOT reflected in ap/buildrepo, but should remove false positives
				// and hopefully not cause false negatives
				for _, provider := range providers {
					if strings.Contains(provider, "-stage0") || strings.Contains(provider, "-bootstrap") {
						chosenProvider = provider
					}
				}

				deps[i] = chosenProvider
			}
		}
	}

	for _, pkg := range pkgs {
		recurse([]string{pkg})
	}

	fmt.Println("done!")
	os.Exit(exitCode)
}

func recurse(history []string) {
	current := history[len(history)-1]
	checked[current] = true

	for _, dep := range dependencies[current] {
		if dep == current {
			continue
		}
		amended := append(history, dep)
		for i, prev := range history {
			if prev == dep {
				fmt.Println("circular dependency:", amended[i:])
				exitCode = 1
				return
			}
		}

		if checked[dep] {
			continue
		}

		recurse(amended)
	}
}

func traverse() chan *Entry {
	ch := make(chan *Entry, 10)
	go func() {
		err := filepath.WalkDir(".", func(path string, d fs.DirEntry, err error) error {
			if err != nil || !d.IsDir() {
				return err
			}

			if d.Name() == "src" || d.Name() == "pkg" {
				return filepath.SkipDir
			}

			ch <- &Entry{
				path:  path,
				entry: d,
			}
			return nil
		})
		if err != nil {
			fmt.Printf("walkdir: %s\n", err)
			os.Exit(1)
		}
		close(ch)
	}()
	return ch
}
